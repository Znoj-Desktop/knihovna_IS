﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace dbInterface
{
    public abstract class IUzivatel
    {
        public abstract Collection<Uzivatel> Select();
    }
}
