﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Model;
using dbInterface;

namespace MSdb
{
    public class AutorTable : IAutor
    {
        public static String TABLE_NAME = "Autor";
        public String SQL_SELECT = "Select * from Autor";

        public AutorTable()
        {
        }


        public override Collection<Autor> Select()
        {
            Database db = new Database();
            db.Connect();

            SqlCommand command = db.CreateCommand(SQL_SELECT);
            
            SqlDataReader reader = db.Select(command);

            Collection<Autor> Autor = Read(reader);
            reader.Close();
            db.Close();
            return Autor;
        }

        private Collection<Autor> Read(SqlDataReader reader)
        {
            Collection<Autor> Autor = new Collection<Autor>();

            while (reader.Read())
            {
                Autor _Autor = new Autor();
                _Autor.IdAutor = reader.GetInt32(0);
                _Autor.Jmeno = reader.GetString(1);
                _Autor.Prijmeni = reader.GetString(2);
                _Autor.Narozeni = reader.GetDateTime(3);
                _Autor.Umrti = reader.GetDateTime(4);
                _Autor.Zivotopis = reader.GetString(5);
                _Autor.Fotka = reader.GetString(6);
                Autor.Add(_Autor);
            }
            return Autor;
        }
    }
}