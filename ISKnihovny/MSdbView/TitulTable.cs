﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Model;
using dbInterface;

namespace MSdb
{
    public class TitulTable : ITitul
    {
        public static String TABLE_NAME = "Titul";
        public String SQL_SELECT = "Select * from Titul";
        private List<Autor> autori;
        
        public TitulTable(List<Autor> Lautor)
        {
            this.autori = Lautor;
        }

        public TitulTable(Autor autor)
        {
            this.autori.Add(autor);
        }

        public override Collection<Titul> Select()
        {
            Database db = new Database();
            db.Connect();

            SqlCommand command = db.CreateCommand(SQL_SELECT);

            SqlDataReader reader = db.Select(command);

            Collection<Titul> Titul = Read(reader);
            reader.Close();
            db.Close();
            return Titul;
        }

        private Collection<Titul> Read(SqlDataReader reader)
        {
            Collection<Titul> Titul = new Collection<Titul>();

            while (reader.Read())
            {
                Titul _titul = new Titul();
                _titul.IdTitul = reader.GetInt32(0);
                _titul.Nazev = reader.GetString(1);
                _titul.Isbn = reader.GetString(2);
                _titul.Zanr = reader.GetString(3);
                _titul.Volnych = reader.GetInt32(4);
                _titul.Celkem = reader.GetInt32(5);
                _titul.Popis = reader.GetString(6);
                _titul.Obrazek = reader.GetString(7);
                Titul.Add(_titul);
            }
            return Titul;
        }
    }
}