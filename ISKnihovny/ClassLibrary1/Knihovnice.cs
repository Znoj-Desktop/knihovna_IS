﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
    public class Knihovnice
    {
        public long IdKnihovnice { get; set; }
        public string Jmeno { get; set; }
        public string Prijmeni { get; set; }
        public string Login { get; set; }
        public string Heslo { get; set; }
    }
}