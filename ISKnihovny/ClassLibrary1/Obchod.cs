﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
    public class Obchod
    {
        public long Id { get; set; }
        public string Jmeno { get; set; }
        public string Adresa { get; set; }
    }
}