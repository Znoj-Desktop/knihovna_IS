/*
Created: 5.11.2013
Modified: 5.11.2013
Model: lm_vis
Database: MS SQL Server 2012
*/









-- Drop keys for tables section -------------------------------------------------

ALTER TABLE [U�ivatel] DROP CONSTRAINT [Unique_Identifier10]
go
ALTER TABLE [Rezervace] DROP CONSTRAINT [Unique_Identifier6]
go
ALTER TABLE [Knihovnice] DROP CONSTRAINT [Unique_Identifier5]
go
ALTER TABLE [Obchod] DROP CONSTRAINT [Unique_Identifier3]
go
ALTER TABLE [Titul] DROP CONSTRAINT [Unique_Identifier2]
go
ALTER TABLE [Autor] DROP CONSTRAINT [Unique_Identifier1]
go





-- Drop tables section ---------------------------------------------------

DROP TABLE [Titul_Obchod]
go
DROP TABLE [Autor_Titul]
go
DROP TABLE [U�ivatel]
go
DROP TABLE [Rezervace]
go
DROP TABLE [Knihovnice]
go
DROP TABLE [U�ivatel]
go
DROP TABLE [Obchod]
go
DROP TABLE [Titul]
go
DROP TABLE [Autor]
go

-- Create tables section -------------------------------------------------

-- Table Autor

CREATE TABLE [Autor]
(
 [IdAutor] Int NOT NULL,
 [Jm�no] Varchar(30) NOT NULL,
 [P��jmen�] Varchar(30) NOT NULL,
 [Norozen�] Date NOT NULL,
 [�mrt�] Datetime NULL,
 [�ivotopis] Varchar(1000) NULL,
 [Fotka] Varchar(30) NULL
)
go

-- Add keys for table Autor

ALTER TABLE [Autor] ADD CONSTRAINT [Unique_Identifier1] PRIMARY KEY ([IdAutor])
go

-- Table Titul

CREATE TABLE [Titul]
(
 [IdTitul] Int NOT NULL,
 [N�zev] Varchar(30) NOT NULL,
 [ISBN] Varchar(20) NOT NULL,
 [��nr] Varchar(30) NOT NULL,
 [Voln�ch] Int NOT NULL,
 [Celkem] Int NOT NULL,
 [Popis] Varchar(1000) NULL,
 [Obr�zek] Varchar(30) NULL
)
go

-- Add keys for table Titul

ALTER TABLE [Titul] ADD CONSTRAINT [Unique_Identifier2] PRIMARY KEY ([IdTitul])
go

-- Table Obchod

CREATE TABLE [Obchod]
(
 [IdObchod] Int NOT NULL,
 [Jm�no] Varchar(30) NOT NULL,
 [Adresa] Varchar(250) NOT NULL
)
go

-- Add keys for table Obchod

ALTER TABLE [Obchod] ADD CONSTRAINT [Unique_Identifier3] PRIMARY KEY ([IdObchod])
go

-- Table U�ivatel

CREATE TABLE [U�ivatel]
(
)
go

-- Table Knihovnice

CREATE TABLE [Knihovnice]
(
 [IdKnihovnice] Int NOT NULL,
 [Jm�no] Varchar(30) NOT NULL,
 [P��jmen�] Varchar(30) NOT NULL,
 [Login] Varchar(6) NOT NULL,
 [Heslo] Varchar(10) NOT NULL
)
go

-- Add keys for table Knihovnice

ALTER TABLE [Knihovnice] ADD CONSTRAINT [Unique_Identifier5] PRIMARY KEY ([IdKnihovnice])
go

-- Table Rezervace

CREATE TABLE [Rezervace]
(
 [IdRezervace] Int NOT NULL,
 [Vyd�no] Datetime NULL,
 [Vr�ceno] Datetime NULL,
 [IdTitul] Int NOT NULL,
 [IdUzivatel] Int NOT NULL
)
go

-- Add keys for table Rezervace

ALTER TABLE [Rezervace] ADD CONSTRAINT [Unique_Identifier6] PRIMARY KEY ([IdRezervace],[IdTitul],[IdUzivatel])
go

-- Table U�ivatel

CREATE TABLE [U�ivatel]
(
 [IdUzivatel] Int NOT NULL,
 [Jm�no] Varchar(30) NOT NULL,
 [P��jmen�] Varchar(30) NOT NULL,
 [Heslo] Varchar(30) NOT NULL,
 [Adresa] Varchar(120) NULL,
 [Telefon] Bigint NULL
)
go

-- Add keys for table U�ivatel

ALTER TABLE [U�ivatel] ADD CONSTRAINT [Unique_Identifier10] PRIMARY KEY ([IdUzivatel])
go

-- Table Autor_Titul

CREATE TABLE [Autor_Titul]
(
 [IdAutor] Int NOT NULL,
 [IdTitul] Int NOT NULL
)
go

-- Table Titul_Obchod

CREATE TABLE [Titul_Obchod]
(
 [IdTitul] Int NOT NULL,
 [IdObchod] Int NOT NULL
)
go



