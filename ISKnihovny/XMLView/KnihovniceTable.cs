﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using dbInterface;
using Model;
namespace XMLdb
{
    public class KnihovniceTable : IKnihovnice
    {
        public override Collection<Knihovnice> Select()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("d:\\programming\\knihovna_IS\\ISKnihovny\\XMLView\\db.xml");
            XmlNode root = doc.DocumentElement;
            Collection<Knihovnice> kolekce = new Collection<Knihovnice>();

            foreach (XmlNode node in root.ChildNodes)
            {
                if (node.Name == "knihovnice")
                {
                    XmlElement knihovnice = (XmlElement)node;
                    Knihovnice k = new Knihovnice();
                    k.IdKnihovnice = int.Parse(knihovnice.GetAttribute("IdKnihovnice"));
                    k.Jmeno = knihovnice.GetElementsByTagName("jmeno")[0].InnerText;
                    k.Prijmeni = knihovnice.GetElementsByTagName("prijmeni")[0].InnerText;
                    k.Login = knihovnice.GetElementsByTagName("login")[0].InnerText;
                    k.Heslo = knihovnice.GetElementsByTagName("heslo")[0].InnerText;

                    kolekce.Add(k);
                }
            }

            return kolekce;
        }
    }
    
}
