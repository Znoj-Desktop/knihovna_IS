- [**Description**](#description)
- [**Technology**](#technology)
- [**Year**](#year)
- [**6 základních otázek**](#6-zakladnich-otazek)
- [**Funkční specifikace**](#funkcni-specifikace)
  - [**Use case diagram**](#use-case-diagram)
  - [**Diagram aktivit**](#diagram-aktivit)
  - [**Funkční požadavky**](#funkcni-pozadavky)
  - [**Nefunkční požadavky**](#nefunkcni-pozadavky)
- [**Technická specifikace**](#technicka-specifikace)
- [**Prototyp uživatelského rozhraní**](#prototyp-uzivatelskeho-rozhrani)
- [**Prototyp architektury systému**](#prototyp-architektury-systemu)
- [**Návrh doménového modelu**](#navrh-domenoveho-modelu)
  - [**Třídní diagram**](#tridni-diagram)
  - [**Logický model**](#logicky-model)
  - [**ER diagram**](#er-diagram)
  - [**Sekvenční diagram**](#sekvencni-diagram)
- [**Použité vzory**](#pouzite-vzory)
- [**Konzistentní funkční část vybraného informačního systému**](#konzistentni-funkcni-cast-vybraneho-informacniho-systemu)
- [**Screenshot**](#screenshot)
- [**Doc**](#doc)
- [**SQL script**](#sql-script)


### **Description**
Vytvoření IS knihovny, kde lze procházet a vyhledávat knihy, které knihovna nabízí, zobrazovat detaily o autorovi včetně fotky, životopisu a knih, které se od něj v knihovně nachází, zobrazovat detaily o jednotlivých titulech se zobrazením obálky knihy a krátkého obsahu. U vybraných titulů bude odkaz na koupi dané knihy.
Knihy lze v knihovně rezervovat, pokud jsou nějaké k dispozici a zjistit celkový počet knih jednotlivých titulů, které má knihovna k dispozici.

---
### **Technology**
C#

---
### **Year**
2013

---
### **6 základních otázek**

CO?	Evidence knih, autorů a jejich detailů, rezervace knih s odkazy k jejich nákupu  
JAK?	Vyhledávání dle různých kritérií a rezervace knih, přidávání a úprava knih  
KDE?	Na webu vyhledávání a rezervace, v knihovně přidávání a úprava knih  
KDO?	Uživatel, knihovnice  
KDY?	Při potřebě rezervovat si nějakou knihu, při ověřování její dostupnosti, při koupi vybrané knihy, při koupení nové knihy do knihovny, při vkládání nových partnerů pro odkazy k vybraným knihám  
PROČ?	Systém umožňuje dělat reklamu nasmlouvaným partnerům prodávajícím knihy a zobrazovat o knihách více informací než IS jiných knihoven  

---
### **Funkční specifikace**
#### **Use case diagram**
![](./README/use_case.png)

#### **Diagram aktivit**
Vyhledávání uživatelem  
![](./README/aktiv_vyhledavani.png)

#### **Funkční požadavky**
_FR1: Systém bude umožňovat přihlášení_  
Desc: Uživatel i Knihovnice se mohou přihlásit do systému  
	Priority: 1  

_FR2. Systém bude umožňovat procházení jednotlivých titulů_  
Desc: Systém zobrazí uživateli seznam všech dostupných titulů. Tento seznam bude obsahovat jméno autora, název titulu, žánr  
	Priority: 2  
	Dependency: 1  

_FR3: Systém bude umožňovat rezervování knih_  
Desc: Systém zobrazí uživateli pro každý titul počet titulů vlastněných knihovnou a počet dostupných titulů k vypůjčení. Dále pak také bude k dispozici u každého titulu tlačítko pro realizaci rezervace daného titulu  
	Priority: 3  
	Dependency: 2  

_FR4: Systém bude umožňovat vydání knih_  
Desc: Systém zobrazí knihovnici záložku Vydání knih, po jejím vybrání pak seznam titulů k vydání. Systém zobrazí tlačítko pro vydání daného titulu.  
	Priority: 2  
	Dependency: 1  

_FR5: Systém bude umožňovat vrácení knih_  
Desc: Systém zobrazí knihovnici záložku Vrácení knih, po jejím vybrání pak seznam titulů k vydání. Systém zobrazí tlačítko pro vrácení daného titulu.  
	Priority: 2  
	Dependency: 4  

_FR6. Systém bude umět vyhledávat knihy podle různých kritérií_  
Desc: Systém umožní knihovnici vyhledání podle autora, názvu knihy, ISBN, zákazníka. Uživateli pak vyhledávání podle autora, názvu knihy, žánru.  
    Priority: 2  
	Dependency: 1  

_FR7: Systém bude umožňovat detailní zobrazení vybraného titulu_  
Desc: Systém nabídne uživateli pro vybrané tituly možnost zobrazit detaily vybraného titulu. V těchto detailech bude jméno titulu, autor, fotka titulu, popis titulu, počet volných titulů, počet titulů vlastněných knihovnou a možnost rezervace vybraného titulu.  
	Priority: 4  
	Dependency: 2  

_FR8: Systém bude umožňovat detailní zobrazení vybraného autora_  
Desc: Systém nabídne uživateli pro vybrané autory možnost zobrazit detaily vybraného autora. V těchto detailech bude jméno autora, fotografie autora, životopis autora, pro každé jeho dílo vlastněné knihovnou jeho jméno, počet volných titulů, počet titulů vlastněných knihovnou a možnost rezervace vybraného titulu.  
	Priority: 4  
	Dependency: 2  

_FR9: Systém bude umožňovat přesměrování na obchod s vybraným titulem_  
Desc: Pro vybrané tituly se zobrazí uživateli možnost přesměrovat se na obchod s daným titulem dostupným k zakoupení  
	Priority: 5  
	Dependency: 2  

_FR10: Systém bude umožňovat zobrazení historie_  
Desc: Knihovnice bude mít k dispozici záložku historie. Po jejím vybrání se jí zobrazí seznam titulů s názvem díla, autorem, uživatelem, který si daný titul vybral, případně vrátil. Také bude obsahovat datum výpůjčky, případně datum vrácení daného titulu s možností vybrání daného data a tím jeho vymazání. Toto bude sloužit jako „tlačítko zpět“ pro vypůjčení, případně vrácení vybraného titulu.  
	Priority: 3  
	Dependency: 4, 5  
	
#### **Nefunkční požadavky**
_Schopnost údržby systému:_  
	V systému je snadná výměna prezentační vrstvy (View) a datová vrstva  
	
_Výkon:_  
	Systém umožňuje zpracování nejméně 5000 operací (výpůjček, vrácení knih) denně a nejméně 5000 rezervací knih  
	Maximální doba pro vyhledání knihy bude v řádu sekund.  
	
_Vhodnost k použití:_  
	Jednoduché uživatelské rozhraní, intuitivní práce se systémem.  

---
### **Technická specifikace**
_.NET_  
-	Jednotná architektura pro klienta i server. Lze mít vše od 1 výrobce  
-	VS.NET je plně integrované se serverovými systémy. Je otevřené třetím stranám a integruje velké množství programovacích jazyků (C#, VB.NET, C++.NET a další)  
-	Nabízí integraci COM komponent (Windows, Ofﬁce, atd.)  
-	.NET je proprietární řešení Microsoftu – podporuje OS Windows (případně Linux v rámci Mono projektu)  
  	
_J2EE_  
-	Vývoj v jazyce Java  
-	Lze použít ve všech OS, kde je k dispozici JVM.  
-	Open Source  
-	Stabilita (mnoho let vývoje)  

Zvolím .NET z toho důvodu, že Microsoft nabízí komplexní řešení a dobrou dokumentaci. Vše lze nalézt na jednom místě.  
  
Cena serveru: 10 000,-  
Náklady na použití vývojových nástrojů: 700,-	

---
### **Prototyp uživatelského rozhraní**  
![](./README/IS_KNIHOVNA%20knihovnice1.png)  

![](./README/IS_KNIHOVNA%20knihovnice2.png)  

![](./README/IS_KNIHOVNA%20knihovnice3.png)  

![](./README/IS_KNIHOVNA%20zakaznik.png)  

![](./README/IS_KNIHOVNA%20zakaznik2.png)  

![](./README/detail_kniha.png)  

![](./README/detail_autor.png)  

---
### **Prototyp architektury systému**
![](./README/diagram_komponent.png)  

---
### **Návrh doménového modelu**
#### **Třídní diagram**
![](./README/tridni.png)  

#### **Logický model**
![](./README/lm_vis.png)  

#### **ER diagram**
![](./README/er_vis.png)  

#### **Sekvenční diagram**
![](./README/sekv.png)  

---
### **Použité vzory**
_Lazy Loading + Asociation Table Mapping + Active Record_  
    při zobrazování detailu autora nebo detailu titulu  
    ![](./README/lazy_loading.png)  
		
_Transaction Script_  
    použití při zaznamenání výpůjčky, zaznamenání vrácení knihy, změna v historii výpůjček a vrácení  
_Identity Field_  
    použit u všech dat  
_Foreign Key Mapping_  
    vztah Obchod – Titul, vztah Autor – Titul,  

---
### **Konzistentní funkční část vybraného informačního systému**
s vysokým důrazem na architekturu a návrh (rozvrstvení, návrh v jednotlivých vrstvách, vzory). Předpokládá se implementace alespoň tří use case, dvou jednoduchých uživatelských rozhraní na různých platformách (tlustý klient, webový klient, mobilní klient apod.) a použití dvou způsobů uložení dat (SQL databáze, XML apod.).

---
### **Screenshot**
![console](./README/console.png)  

![webová aplikace](./README/webApp.png)  

---
### **Doc**  
[ISknihovna1_7 docx](/README/ISknihovna1_7.docx)  
[ISknihovna1_7 pdf](/README/ISknihovna1_7.pdf)

---
### **SQL script**  
[knihovna.SQL](/README/knihovna.SQL)