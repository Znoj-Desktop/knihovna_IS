﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Model;
using dbInterface;

namespace MSdb
{
    public class KnihovniceTable : IKnihovnice
    {
        public static String TABLE_NAME = "Knihovnice";
        public String SQL_SELECT = "Select * from Knihovnice";

        public KnihovniceTable()
        {
        }

        
        public override Collection<Knihovnice> Select()
        {
            Database db = new Database();
            db.Connect();

            SqlCommand command = db.CreateCommand(SQL_SELECT);
            
            SqlDataReader reader = db.Select(command);

            Collection<Knihovnice> knihovnice = Read(reader);
            reader.Close();
            db.Close();
            return knihovnice;
        }

        private Collection<Knihovnice> Read(SqlDataReader reader)
        {
            Collection<Knihovnice> knihovnice = new Collection<Knihovnice>();

            while (reader.Read())
            {
                Knihovnice _knihovnice = new Knihovnice();
                _knihovnice.IdKnihovnice = reader.GetInt32(0);
                _knihovnice.Jmeno = reader.GetString(1);
                _knihovnice.Prijmeni = reader.GetString(2);
                _knihovnice.Login = reader.GetString(3);
                _knihovnice.Heslo = reader.GetString(4);
                knihovnice.Add(_knihovnice);
            }
            return knihovnice;
        }
    }
}