﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
    public class Titul
    {
        public long IdTitul { get; set; }
        public string Nazev { get; set; }
        public string Isbn { get; set; }
        public string Zanr { get; set; }
        public int Volnych { get; set; }
        public int Celkem { get; set; }
        public string Popis { get; set; }
        public string Obrazek { get; set; }
    }
}