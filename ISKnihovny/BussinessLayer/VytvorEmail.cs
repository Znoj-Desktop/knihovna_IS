﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace BussinessLayer
{
    public class VytvorEmail
    {
        public List<string> email(List<Knihovnice> list)
        {
            List<string> l = new List<string>();
            foreach (Knihovnice k in list)
            {
                l.Add(k.Jmeno + "." + k.Prijmeni + "@vsb.cz");
            }

            return l;
        }
    }
}
