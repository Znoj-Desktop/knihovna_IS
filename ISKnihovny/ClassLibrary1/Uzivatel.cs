﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
    public class Uzivatel
    {
        public long Id { get; set; }
        public string Jmeno { get; set; }
        public string Prijmeni { get; set; }
        public string Heslo { get; set; }
        public string Adresa { get; set; }
        public long Telefon { get; set; }
    }
}