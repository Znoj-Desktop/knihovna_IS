﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
    public class Autor
    {
        public long IdAutor { get; set; }
        public string Jmeno { get; set; }
        public string Prijmeni { get; set; }
        public DateTime Narozeni { get; set; }
        public DateTime Umrti { get; set; }
        public string Zivotopis { get; set; }
        public string Fotka { get; set; }
    }
}