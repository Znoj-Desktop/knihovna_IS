﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Model.Knihovnice>>" %>

<!DOCTYPE html>
<script runat="server">

    protected void onClick(object sender, EventArgs e)
    {
        //a.Attributes.Add("style", "display:block;");
    }
</script>

<script type="text/javascript">
    function my() {
        var element = document.getElementById("mujDIV");
        if(element.style.display == "block") {
            element.style.display = "none";
        } else {
            element.style.display = "block";
        }
    }
</script>


<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>Vyhledavani</title>
    <link rel="stylesheet" href="../../Content/main.css">
</head>
<body>
    <div id="main">
    <form id="form1" runat="server">
        <div class="row">
        
            <asp:Button class="btn btn-info btn-lg" ID="Button1" runat="server" Text="Vyhledávání" />
            <asp:Button ID="Button2" style="margin-left:15px;" runat="server" class="btn btn-default btn-lg" Text="Rezervace" />
            <asp:Button ID="Button3" runat="server" class="btn btn-default btn-lg" style="float:right;width:116px;" Text="Odhlášení"/>
        
        </div>
        <div class="row">
            <asp:TextBox class="form-control input-lg" ID="TextBox1" runat="server" style="float:left;width:84%"></asp:TextBox>
            <asp:Button ID="Button4" class="btn btn-info btn-lg" runat="server"  Text="Vyhledat" OnClick="onClick" style="float:right;width:116px;"/>
        </div>
       



        <table class="table">
        <tr>
            <th>
                <%: Html.DisplayNameFor(model => model.IdKnihovnice) %>
            </th>
            <th>
                <%: Html.DisplayNameFor(model => model.Jmeno) %>
            </th>
            <th>
                <%: Html.DisplayNameFor(model => model.Prijmeni) %>
            </th>
            <th>
                <%: Html.DisplayNameFor(model => model.Login) %>
            </th>
            <th>
                <%: Html.DisplayNameFor(model => model.Heslo) %>
            </th>
            <th>
                <b>email</b>
            </th>
            <th></th>
        </tr>
    
    <%  int i = 0;
        foreach (var item in Model) { %>
        <tr>
            <td>
                <%: Html.DisplayFor(modelItem => item.IdKnihovnice) %>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.Jmeno) %>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.Prijmeni) %>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.Login) %>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.Heslo) %>
            </td>
            <td>
                <%: @ViewBag.Emaily[i++] %>
            </td>
            <td>
            </td>
        </tr>
    <% } %>
    
    </table>
    </form>
    </div>
</body>
</html>
