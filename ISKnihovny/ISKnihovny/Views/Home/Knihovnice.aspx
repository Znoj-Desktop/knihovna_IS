﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Model.Knihovnice>>" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>Knihovnice</title>
</head>
<body>
    <p>
        <%: Html.ActionLink("Create New", "Create") %>
    </p>
    <table>
        <tr>
            <th>
                <%: Html.DisplayNameFor(model => model.IdKnihovnice) %>
            </th>
            <th>
                <%: Html.DisplayNameFor(model => model.Jmeno) %>
            </th>
            <th>
                <%: Html.DisplayNameFor(model => model.Prijmeni) %>
            </th>
            <th>
                <%: Html.DisplayNameFor(model => model.Login) %>
            </th>
            <th>
                <%: Html.DisplayNameFor(model => model.Heslo) %>
            </th>
            <th>
                <b>email</b>
            </th>
            <th></th>
        </tr>
    
    <%  int i = 0;
        foreach (var item in Model) { %>
        <tr>
            <td>
                <%: Html.DisplayFor(modelItem => item.IdKnihovnice) %>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.Jmeno) %>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.Prijmeni) %>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.Login) %>
            </td>
            <td>
                <%: Html.DisplayFor(modelItem => item.Heslo) %>
            </td>
            <td>
                <%: @ViewBag.Emaily[i++] %>
            </td>
            <td>
                <%: Html.ActionLink("Edit", "Edit", new { /* id=item.PrimaryKey */ }) %> |
                <%: Html.ActionLink("Details", "Details", new { /* id=item.PrimaryKey */ }) %> |
                <%: Html.ActionLink("Delete", "Delete", new { /* id=item.PrimaryKey */ }) %>
            </td>
        </tr>
    <% } %>
    
    </table>

    
</body>
</html>
